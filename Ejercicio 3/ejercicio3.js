var numeroDNI = window.prompt("Escribe tu número de DNI");
var letraDNI = window.prompt("Escribe la letra del DNI");
var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 
			  'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
if (numeroDNI>0 && numeroDNI<=99999999) {
    var indiceLetraDNI = numeroDNI % 23;
    var letraBuena = letras[indiceLetraDNI];
    if (letraDNI === letraBuena) {
        console.log("DNI válido");
    } else { 
        console.log("DNI incorrecto");
    }
}   else {
    console.log("El número proporcionado no es válido");
}
